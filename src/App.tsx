import React, { useEffect, useMemo, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Grid from './Grid';

function App() {

  const [playing, setPlaying] = useState(false)
  const [reset, setReset] = useState(false)
  const triggerReset = () => {
    setPlaying(false)
    setReset(true)
  }

  useEffect(() => {
    if (reset) setReset(false)
  }, [reset])

  return (
    <div className="App">
      <header className="App-header">
        <h1>Conway's game of life</h1>
        <div className="controls">
          <button onClick={() => setPlaying(!playing)}>
            {playing ? 'Pause' : 'Play'}
          </button>
          <button onClick={() => triggerReset()}>Restore</button>
        </div>
      </header>
      <div id="container">
        <Grid playing={playing} setPlaying={setPlaying} reset={reset} />       
      </div>
    </div>
  );
}

export default App;
