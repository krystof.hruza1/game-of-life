import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
const SIZE = 40
const defaultPattern = {
  17: [21, 22, 23],
  18: [23],
  19: [21, 22, 23]
}

type StateType = boolean[][]
type Cell = CellCoordinates & { alive: boolean }
type CellCoordinates = { x: number, y: number }
interface GridProps {
  playing: boolean
  setPlaying: (playing: boolean) => void
  reset: boolean
}

function getDefaultState(): StateType {
  let state: StateType = []
  for (let i = 0; i < SIZE; i++) {
    state.push([])
    for (let n = 0; n < SIZE; n++) {
      const alive = defaultPattern[i+1 as keyof typeof defaultPattern]?.find(c => c-1 === n) 
      state[i].push(!!alive)
    }
  }

  return state
}

function isCellAlive(state: StateType, cell: Cell): { alive: boolean, destroy?: CellCoordinates } {
  let neighbors = []

  for (let i = cell.x - 1; i <= cell.x + 1; i++) {
    const itemOver = state[cell.y - 1]?.[i]
    const itemBelow = state[cell.y + 1]?.[i]
    
    if (itemOver) neighbors.push({x: i, y: cell.y - 1})
    if (itemBelow) neighbors.push({x: i, y: cell.y + 1})
  }

  if (state[cell.y]?.[cell.x-1]) neighbors.push({x: cell.x-1, y: cell.y})
  if (state[cell.y]?.[cell.x+1]) neighbors.push({x: cell.x+1, y: cell.y})

  if (cell.alive) {
    if (neighbors.length >= 2 && neighbors.length <= 3) {
      return { alive: true }
    } else {
      return { alive: false }
    }
  } else {
    if (neighbors.length === 3) {
      return { alive: true }
    }
    else if (neighbors.length === 2) {
      const randomIndex = Math.round(Math.random())
      // SWITCH: Uncomment to enable the last, non-standard, rule
      return { alive: false/*, destroy: neighbors[randomIndex]*/}
    } else {
      return {alive: false}
    }
  }
}

export default function Grid({playing, setPlaying, reset}: GridProps) {
  const [state, setState] = useState(getDefaultState())

  const getNewState = (prevState: StateType): StateType => {
    let toDestroy: CellCoordinates[] = []
    let newState = prevState.map((row, y) => {
      return row.map((cell, x) => {
        const result = isCellAlive(prevState, { x, y, alive: cell })
        if (result.destroy) {toDestroy.push(result.destroy)}
        return result.alive
      })
      
    })

    toDestroy.forEach((cell) => {
      newState[cell.y][cell.x] = false
    })

    return newState
  }

  useEffect(() => {
    const interval = setInterval(() => setState(prevState => getNewState(prevState)), 40);
    if(!playing) clearInterval(interval)
    
    return () => clearInterval(interval);

  }, [playing])

  useMemo(() => {
    if (reset) {
      setState(getDefaultState())
    }
  }, [reset])

  return (
    <table id="game">
      <tbody>
        <tr>
          <td />
          {state.map((r, i) => <th key={i}>{i+1}</th>)}
        </tr>
        {state.map((row, rowIndex) => {
          return (
            <tr key={rowIndex}>
              <td>{rowIndex+1}</td>
              {row.map((cell, cellIndex) => {
                return (<td className={cell ? 'alive' : undefined} key={cellIndex} />)
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}