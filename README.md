Projekt je postaven na základě standardního CRA+TS templatu.

Pro rozběhnutí stačí projekt naklonovat, nainstalovat závislosti (`yarn install`) a rozběhnout pomocí `yarn start`.

Z časových důvodů k aplikaci nejsou žádné testy a UI je minimální. Ze stejného důvodu nejsou nutně dodržovány best practices ohledně dělení kódu a některé další.

Tak jak to chápu je zadání buď nekompletní (chybějící instrukce pro řešení konfliktních pravidel), nebo chybné (konflikty mezi pravidly) Vzhledem k tomu, že nebylo možné verifikovat přesnou povahu problému, je v kódu implementované řešení, kde páté pravidlo přebírá prioritu v případě konfliktu s prvním pravidlem, ale zakomentováním property 'destroy' v Souboru Grid.tsx na řádce 58 je možné efektivně přepnout do stavu, kdy se program chová pouze podle prvních 4 pravidel. (současný stav)